import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrowsShrinkBasisComponent } from './grows-shrink-basis.component';

describe('GrowsShrinkBasisComponent', () => {
  let component: GrowsShrinkBasisComponent;
  let fixture: ComponentFixture<GrowsShrinkBasisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrowsShrinkBasisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrowsShrinkBasisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
