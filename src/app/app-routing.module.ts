import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlignContentComponent } from './align-content/align-content.component';
import { AlignSelfComponent } from './align-self/align-self.component';
import { AlignmentComponent } from './alignment/alignment.component';
import { FlexBasisComponent } from './flex-basis/flex-basis.component';
import { FlexGrowComponent } from './flex-grow/flex-grow.component';
import { FlexWrapComponent } from './flex-wrap/flex-wrap.component';
import { FlowDirectionComponent } from './flow-direction/flow-direction.component';
import { GridsComponent } from './grids/grids.component';
import { FlexShrinkComponent } from './flex-shrink/flex-shrink.component';
import { GrowsShrinkBasisComponent } from './grows-shrink-basis/grows-shrink-basis.component';
import { HexagonComponent } from './hexagon/hexagon.component';
import { HolyGrailComponent } from './holy-grail/holy-grail.component';
import { JustifyComponent } from './justify/justify.component';
import { MediaObjectsComponent } from './media-objects/media-objects.component';
import { OrderComponent } from './order/order.component';
import { VerticalComponent } from './vertical/vertical.component';

const routes: Routes = [
  {
    path: 'align-content',
    component: AlignContentComponent
  },
  {
    path: 'align-self',
    component: AlignSelfComponent
  },
  {
    path: 'alignment',
    component: AlignmentComponent
  },
  {
    path: 'flex-basis',
    component: FlexBasisComponent
  },
  {
    path: 'flex-grow',
    component: FlexGrowComponent
  },
  {
    path: 'flex-shrink',
    component: FlexShrinkComponent
  },
  {
    path: 'flex-wrap',
    component: FlexWrapComponent
  },
  {
    path: 'flow-direction',
    component: FlowDirectionComponent
  },
  {
    path: 'grids',
    component: GridsComponent
  },
  {
    path: 'grows-shrink-basis',
    component: GrowsShrinkBasisComponent
  },
  {
    path: 'hexagon',
    component: HexagonComponent
  },
  {
    path: 'holy-grail',
    component: HolyGrailComponent
  },
  {
    path: 'justify',
    component: JustifyComponent
  },
  {
    path: 'media-objects',
    component: MediaObjectsComponent
  },
  {
    path: 'order',
    component: OrderComponent
  },
  {
    path: 'vertical',
    component: VerticalComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
